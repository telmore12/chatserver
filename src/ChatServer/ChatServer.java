package ChatServer;

import java.io.*;
import java.net.*;
public class ChatServer {
    public static void main(String[] args){
        ServerSocket server = null;
        try {
            server = new ServerSocket(420);
            server.setReuseAddress(true);
            while (true) {
                Socket client = server.accept();
                System.out.println("New client connected"+ client.getInetAddress().getHostAddress());
  
                ClientHandler clientSock= new ClientHandler(client);
                new Thread(clientSock).start();
            }
        }
        catch (IOException e) {
            System.out.println(e);
        }
        finally {
            if (server != null) {
                try {
                    server.close();
                }
                catch (IOException e) {
                    System.out.println(e);
                }
                }
            }
        }
}
