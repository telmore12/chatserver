package ChatServer;


import ChatLibrary.*;
import java.io.*;
import java.net.*;

public class ClientHandler implements Runnable {
    private final Socket clientSocket;
    public ClientHandler(Socket socket){
        this.clientSocket = socket;
    }
    @Override
    public void run(){
        try {
              // get the outputstream of client
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
              // get the inputstream of client
            ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream());
            Message message;
            
            while ((message=(Message) in.readObject())!=null) {
                switch (message.getType()) {
                    case "LOGIN":
                        out.println(message.getUser()+" logged succesfully");
                        System.out.println("USER "+message.getUser()+" logged succesfully");
                        break;
                    case "DATA":
                        out.println("<"+message.getUser()+"> :"+message.getData());
                        break;
                    default:
                        throw new AssertionError();
                }
                System.out.println("ASD");
                message=null;
            }
        }
        catch (IOException | ClassNotFoundException e) {
            System.out.println(e);
        }
        finally {
            try {
                clientSocket.close();
            }
            catch (IOException e) {
                System.out.println(e);
            }
        }
    }
}

